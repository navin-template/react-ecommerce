import { Outlet, Link } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';



function MainMenu() {
  return (
    <Navbar expand="lg" className="bg-secondary-tertiary p-0 ">
      <Container fluid  style={{backgroundColor:"#0438a1"}}>
        
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav className="me-auto  my-lg-0" style={{ maxHeight: '100px' }} navbarScroll>
          <Nav.Link ><Link to="/" style={{color:'#f4f6ff'}}>Home</Link> </Nav.Link>

          <Nav.Link><Link to="/shop" style={{color:'#f4f6ff'}}>Shop</Link> </Nav.Link>

         
           
          <Nav.Link><Link to="/product" style={{color:'#f4f6ff',textDecoration:"none"}}>Products</Link> </Nav.Link>
          <Nav.Link ><Link to="/blog" style={{color:'#f4f6ff'}}>Blog</Link> </Nav.Link>

            
            
        
            <Nav.Link ><Link to="/login"  style={{color:'#f4f6ff'}}>Login </Link> </Nav.Link>
            
            <Nav.Link to="/signup" style={{color:'#f4f6ff'}}><Link to="/signup" style={{color:'#f4f6ff'}}>Signup</Link> </Nav.Link>

          </Nav>
          <Form className="d-flex">
          <i class="fa-solid fa-gear" style={{color: '#f4f7fa'}}></i>
          </Form>
        </Navbar.Collapse>
      </Container>
     
    </Navbar>
  );
}

export default MainMenu;