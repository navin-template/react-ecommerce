import Container from 'react-bootstrap/Container';
import Carousel from 'react-bootstrap/Carousel';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
export default function Product(){
    return(
        <>
       
       <Container>
      <Row className='py-4'>
        <Col md={4}>
          <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/products/1_grande.png?v=1536038068' alt=''/>
       
          <Carousel className=' py-4'>
      <Carousel.Item>
        <Row>
          <Col md={4}><img className="d-block w-100" src="https://ponno-demo.myshopify.com/cdn/shop/products/2_compact.png?v=1536038070" alt="First slide"/></Col>
          <Col md={4}><img className="d-block w-100" src="https://ponno-demo.myshopify.com/cdn/shop/products/1_compact.png?v=1536038068" alt="First slide"/></Col>
          
          <Col md={4}><img className="d-block w-100" src="https://ponno-demo.myshopify.com/cdn/shop/products/3_compact.png?v=1536038072" alt="First slide"/></Col>
          
        </Row>
        
       
      </Carousel.Item>
      <Carousel.Item>
      <Row>
          <Col md={4}><img className="d-block w-100" src="https://ponno-demo.myshopify.com/cdn/shop/products/3_compact.png?v=1536038072" alt="First slide"/></Col>
          <Col md={4}><img className="d-block w-100" src="https://ponno-demo.myshopify.com/cdn/shop/products/4_compact.png?v=1536038074" alt="First slide"/></Col>
          
          <Col md={4}><img className="d-block w-100" src="https://ponno-demo.myshopify.com/cdn/shop/products/2_compact.png?v=1536038070" alt="First slide"/></Col>
          
        </Row>
      </Carousel.Item>
      <Carousel.Item>
      <Row>
          <Col md={4}><img className="d-block w-100" src="https://ponno-demo.myshopify.com/cdn/shop/products/5_compact.png?v=1536038075" alt="First slide"/></Col>
          <Col md={4}><img className="d-block w-100" src="https://ponno-demo.myshopify.com/cdn/shop/products/4_compact.png?v=1536038074" alt="First slide"/></Col>
          
          <Col md={4}><img className="d-block w-100" src="https://ponno-demo.myshopify.com/cdn/shop/products/3_compact.png?v=1536038072" alt="First slide"/></Col>
          
        </Row>

       
      </Carousel.Item>
    </Carousel>
       
       
       
       
       
        </Col>
        <Col md={8}>
          <h3>Accusantium Doloremque</h3>
          <p><i class="fa-regular fa-star" style={{color: '#2461cc'}}></i> <i class="fa-regular fa-star" style={{color: '#2461cc'}}></i> <i class="fa-regular fa-star" style={{color: '#2461cc'}}></i> <i class="fa-regular fa-star" style={{color: '#2461cc'}}></i> <i class="fa-regular fa-star" style={{color: '#2461cc'}}></i></p>
        <h3 className='py-2'>$130.00</h3>
        <hr/>
        <p className='py-3'>
        There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.
        </p>
        <hr/>
        <p className='py-3'>Size :  Xl <samp className='mx-2'> Ml </samp> Xs <samp className='mx-2'>XL </samp>  M</p>
        <p>Material:  <samp className='mx-2' style={{color:'#0438a1'}}>Partex </samp>Partex Wood  <samp className='mx-2' > Glass </samp></p>
        <p className='py-3'>Quantity <Button variant="dark" className='mx-4' style={{borderRadius:'10px'}}>ADD TO CART</Button></p>
        <p><Button variant="dark" className='px-4' style={{borderRadius:'10px'}}>BUY IT NOW</Button></p>
        <p className='py-3'>Tag : black, brown, cyan <br/>
           Guaranteed safe checkout</p>
           <p>
          <Row>
            <Col md={4}>
            <Row>
              <Col md={2}>
              <Card style={{ width: '2rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shopifycloud/shopify/assets/payment_icons/amazon-92e856f82cae5a564cd0f70457f11af4d58fa037cf6e5ab7adf76f6fd3b9cafe.svg" />
      
    </Card>
              </Col>
              <Col md={2}>
              <Card style={{ width: '2rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shopifycloud/shopify/assets/payment_icons/apple_pay-f6db0077dc7c325b436ecbdcf254239100b35b70b1663bc7523d7c424901fa09.svg" />
      
    </Card>
              </Col>
              <Col md={2}>
                 
    <Card style={{ width: '2rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shopifycloud/shopify/assets/payment_icons/bitcoin-e41278677541fc32b8d2e7fa41e61aaab2935151a6048a1d8d341162f5b93a0a.svg" />
      
    </Card>
              </Col>
              <Col md={2}>
              <Card style={{ width: '2rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shopifycloud/shopify/assets/payment_icons/google_pay-c66a29c63facf2053bf69352982c958e9675cabea4f2f7ccec08d169d1856b31.svg" />
      
    </Card>
              </Col>
              <Col md={2}>
              <Card style={{ width: '2rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shopifycloud/shopify/assets/payment_icons/paypal-49e4c1e03244b6d2de0d270ca0d22dd15da6e92cc7266e93eb43762df5aa355d.svg" />
      
    </Card>
              </Col>
              <Col md={2}>
              <Card style={{ width: '2rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shopifycloud/shopify/assets/payment_icons/visa-319d545c6fd255c9aad5eeaad21fd6f7f7b4fdbdb1a35ce83b89cca12a187f00.svg" />
      
    </Card>
              </Col>
            </Row>
            </Col>

          </Row>
            
  
    
  
 
   
   
    
    
    </p>
        </Col>
      </Row>
      <Row>
        <Col md={12}>
        <Card style={{ width: '70rem' }}>
      <Card.Img variant="top"  />
      <Card.Body>
        
        <Card.Text>
        <p className='text-center' style={{fontSize: 'larger'}}> <samp style={{color:'#0438a1'}}> DESCRIPTION</samp> <samp className='px-3'> REVIEWS</samp>  COMMENTS</p>
       <hr/>
       <p className='py-2'>
       Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
       </p>
       <p className='py-2'>
       The s tandard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
       </p>
        </Card.Text>
      
      </Card.Body>
    </Card>

        </Col>
      </Row>
      <Row className='py-4'>
        <h4 className='py-4 text-center'>Related Products</h4>
        <Col xs={12} md={3}>
        <Card style={{ width: '16rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/20_9cd630e4-d064-49ed-870d-5a54f862b007_270X270_crop_center.png?v=1536038787" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
     
        </Col>
        <Col xs={12} md={3}>
        <Card style={{ width: '16rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/19_b560804e-402b-4a43-b322-8ce1aca4ec70_270X270_crop_center.png?v=1536038764" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={3}>
        <Card style={{ width: '16rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/11_ec7b8db4-1aa3-4dec-9117-67ba142c15d6_270X270_crop_center.png?v=1536038514" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={3}>
        <Card style={{ width: '16rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/14_e879664b-7645-45fa-ab7a-257cf61280b8_270X270_crop_center.png?v=1536038532" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
      </Row>
      <Row className='py-4'>
        <h4 className='py-4 text-center'>Custom Products</h4>
        <Col xs={12} md={3}>
        <Card style={{ width: '16rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/7_f6cb367d-9d78-4395-a651-0aab92720a0c_270X270_crop_center.png?v=1536039112" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
     
        </Col>
        <Col xs={12} md={3}>
        <Card style={{ width: '16rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/10_fd74d87b-9fb0-44df-b291-d5bef99d99f0_270X270_crop_center.png?v=1536039131" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={3}>
        <Card style={{ width: '16rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/6_68b5568a-7172-4de3-a436-dd199022098b_270X270_crop_center.png?v=1536039105" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={3}>
        <Card style={{ width: '16rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/14_e94de152-b927-4a9d-9113-54ec69f78c88_270X270_crop_center.png?v=1536039349" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
      </Row>
    </Container>
 
            
        
        
        </>
    )
}