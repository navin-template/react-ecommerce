import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Carousel from 'react-bootstrap/Carousel';
export default function Product(){
    return(
        <>
           
      <Container>
      
      <Row>
        
        <Col md={4} className='py-4'>
            <h5>Availability</h5>
            <hr/>
           <p><input type='checkbox'/>  In stock (41)</p> 
           <p className='py-2'><input type='checkbox'/>Out of stock (5)</p> 

           <h5 className='pt-3'>Product Type</h5>
           <hr/>
           <p><input type='checkbox'/> assorted (11)</p> 
           <p className='py-2'><input type='checkbox'/> box (4)</p> 
           <p><input type='checkbox'/> chair (13)</p> 
           <p className='py-2'><input type='checkbox'/>  pant (1)</p> 
           <p><input type='checkbox'/>  shirt (2)</p> 
           <p className='py-2'><input type='checkbox'/>  table (5)</p> 
           <p><input type='checkbox'/>  table-lamp (5)</p> 
           <p className='py-2'><input type='checkbox'/> watch (2)</p> 

           <h5 className='pt-4'>Color</h5>
           <hr/>
           <p><input type='checkbox'/>  black (11)</p> 
           <p className='py-2'><input type='checkbox'/> black (10)</p> 
           <p><input type='checkbox'/> brown (8)</p> 
           <p className='py-2'><input type='checkbox'/>   cyan (7)</p> 
           <p><input type='checkbox'/>  gold (9)</p> 
           <p className='py-2'><input type='checkbox'/>   gray (13)</p> 
           <p><input type='checkbox'/>  magenta (11)</p> 
           <p className='py-2'><input type='checkbox'/>  orange (10)</p> 
           <p><input type='checkbox'/>   pink (7)</p> 
           <p className='py-2'><input type='checkbox'/>   purple (7)</p> 
           <p><input type='checkbox'/>  red (8)</p> 
           <p className='py-2'><input type='checkbox'/>  yellow (11)</p> 
           <h5 className='pt-4'>Material</h5>
           <hr/>
           <p><input type='checkbox'/>  glass (16)</p> 
           <p className='py-2'><input type='checkbox'/>  partex (18)</p> 
           <p><input type='checkbox'/>  steel (4)</p> 
           <p className='py-2'><input type='checkbox'/>    wood (20)</p> 
           <h5 className='pt-4'>Size</h5>
           <hr/>
           <p><input type='checkbox'/>  xs (21)</p> 
           <p className='py-2'><input type='checkbox'/>    M (13)</p> 
           <p><input type='checkbox'/>   L (13)</p> 
           <p className='py-2'><input type='checkbox'/>  xl (21)</p> 
           <p><input type='checkbox'/>    ml (21)</p> 
           <p className='py-2'><input type='checkbox'/>   X (13)</p> 
          
        </Col>
        <Col md={8}>
        <Row className='py-4'>
        
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/2_270X270_crop_center.png?v=1536038070" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/3_1204506d-99eb-4bcc-983d-2c91ff60e03d_270X270_crop_center.png?v=1536038074" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/4_6d126974-7768-49b0-bf41-d5ae8bc0ee44_270X270_crop_center.png?v=1536038081" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
      </Row>
      <Row className='py-4'>
        
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/5_57f47c87-463b-4f20-8fe3-d694cc389fd8_270X270_crop_center.png?v=1536038088" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/6_edac2b4f-a9a5-4a30-af44-155c3a032aaa_270X270_crop_center.png?v=1536038095" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/7_79616a75-984d-41da-83a2-9e7d042829eb_270X270_crop_center.png?v=1536038204" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
      </Row>

      <Row className='py-4'>
        
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/8_25bdddf6-3709-4512-a4c0-f9caaa4bc965_270X270_crop_center.png?v=1536038209" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/9_fad6524a-d93e-4ba5-acbe-2806a367156d_270X270_crop_center.png?v=1536038215" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/10_c4c339ee-4847-45dd-99bb-9cbc2e65dafe_270X270_crop_center.png?v=1536038220" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
      </Row>


      <Row className='py-4'>
        
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/11_ec7b8db4-1aa3-4dec-9117-67ba142c15d6_270X270_crop_center.png?v=1536038514" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/12_464a92d6-2eb9-400b-9b81-3525ab897a4a_270X270_crop_center.png?v=1536038520" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/13_2b14b252-ecb6-407a-9f46-a47d8128c536_270X270_crop_center.png?v=1536038525" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
      </Row>


      <Row className='py-4'>
        
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/19_b560804e-402b-4a43-b322-8ce1aca4ec70_270X270_crop_center.png?v=1536038764" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/16_be5c992f-95cc-4185-a0d8-18559ed14273_270X270_crop_center.png?v=1536038550" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
        <Col xs={12} md={4}>
        <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/15_50255503-d1b9-4fdf-852b-01aa0e7ee32b_270X270_crop_center.png?v=1536038536" />
      <Card.Body>
     
        <Card.Text className='text-center'>
          
         <p> bulk of the card's content.</p>
         <p> $ 1500.00</p>
         <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
         
        </Card.Text>
       
      </Card.Body>
    </Card>
        </Col>
      </Row>
        </Col>
      </Row>
    </Container>

             
        </>
    )
}