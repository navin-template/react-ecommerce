import Container from 'react-bootstrap/Container';
import Carousel from 'react-bootstrap/Carousel';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
export default function Blog(){
    return(
        <>

<Container>
      <Row className='py-4'>
        <Col md={4}>
            <h5 className='pt-3'>Search</h5>
            <hr/>
        <Form className="d-flex">
            <Form.Control type="search" placeholder="Search" className="me-2" aria-label="Search"/>
            <Button variant="outline-success">Search</Button>
          </Form>

          <h5 className='pt-4'>Custom Menu</h5>
          <hr/>
          <p className='pt-4'>Home </p>
          <p className='py-2'>Shop</p>
          <p>Products</p>
          <p className='py-2'>Blog</p>
          <p>contact</p>
          <p className='py-2'>about us</p>

          <h5 className='pt-4'>Recent Post</h5>
          <hr/>


         <Row>
            <Col md={4}>
                <img className='img-fluid' src="https://ponno-demo.myshopify.com/cdn/shop/articles/12_medium.jpg?v=1535885748" alt=""/>
            </Col>
            <Col md={8}>
                <p>

Temporibus Autem <br/> Quibusdam <br/>Sep 02, 2018

</p>
            </Col>
         </Row>

         <Row className='py-4'>
            <Col md={4}>
                <img className='img-fluid' src="https://ponno-demo.myshopify.com/cdn/shop/articles/9_medium.jpg?v=1535885720" alt=""/>
            </Col>
            <Col md={8}>
                <p>
         
Temporibus Autem <br/> Quibusdam <br/>Sep 02, 2018

</p>
            </Col>
         </Row>

         <Row className='py-4'>
            <Col md={4}>
                <img className='img-fluid' src="https://ponno-demo.myshopify.com/cdn/shop/articles/6_medium.jpg?v=1535885683" alt=""/>
            </Col>
            <Col md={8}>
                <p>
         
Temporibus Autem <br/> Quibusdam <br/>Sep 02, 2018

</p>
            </Col>
         </Row>
         <h5 className='pt-4'>Archive</h5>
         <hr/>

         <b>September 2018</b>
         <p className='py-2'>Temporibus autem quibusdam</p>
         <p className='py-2'>Temporibus autem quibusdam</p>
         <p className='py-2'>Temporibus autem quibusdam</p>
         <p className='py-2'>Temporibus autem quibusdam</p>
         <p>The Standard Chunk Of Lorem Ipsum <br/> Used Since</p>
         <h5 className='pt-4'>Tags</h5>
         <hr/>
         <Row>
            <Col md={4}> <Button variant="dark">DESTOP</Button></Col>
            <Col md={4}> <Button variant="dark">DRON</Button></Col>
         </Row>
         <Row className='py-2'>
            <Col md={4}> <Button variant="dark">HEADPHONE</Button></Col>
            <Col md={4}> <Button variant="dark">LAPTOP</Button></Col>
         </Row>
         <Row className='py-2'>
            <Col md={4}> <Button variant="dark">MOBILE</Button></Col>
         
         </Row>


        </Col>
        <Col md={8}>
            <h3 className='py-4 text-center'>Latest Blog Posts</h3>
            <Row>
                
            <Col md={6}>
                <Card style={{ width: '23rem' }}>
      <Card.Img className='img-fluid' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/articles/12_370X370_crop_center.jpg?v=1535885748" />
      <Card.Body>
        
        <Card.Text>
            <b>TEMPORIBUS AUTEM QUIBUSDAM</b>
        <p className='py-2'>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi...
        </p>
        </Card.Text>
        <Button variant="primary">Read More</Button>
      </Card.Body>
    </Card>
                </Col>

                <Col md={6}>
                <Card style={{ width: '23rem' }}>
      <Card.Img className='img-fluid' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/articles/9_370X370_crop_center.jpg?v=1535885720" />
      <Card.Body>
        
        <Card.Text>
            <b>TEMPORIBUS AUTEM QUIBUSDAM</b>
        <p className='py-2'>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi...
        </p>
        </Card.Text>
        <Button variant="primary">Read More</Button>
      </Card.Body>
    </Card>
                </Col>

            </Row>
            
            <Row className='py-4'>
                
                <Col md={6}>
                    <Card style={{ width: '23rem' }}>
          <Card.Img className='img-fluid' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/articles/6_370X370_crop_center.jpg?v=1535885683" />
          <Card.Body>
            
            <Card.Text>
                <b>TEMPORIBUS AUTEM QUIBUSDAM</b>
            <p className='py-2'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi...
            </p>
            </Card.Text>
            <Button variant="primary">Read More</Button>
          </Card.Body>
        </Card>
                    </Col>
    
                    <Col md={6}>
                    <Card style={{ width: '23rem' }}>
          <Card.Img className='img-fluid' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/articles/3_370X370_crop_center.jpg?v=1535885660" />
          <Card.Body>
            
            <Card.Text>
                <b>TEMPORIBUS AUTEM QUIBUSDAM</b>
            <p className='py-2'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi...
            </p>
            </Card.Text>
            <Button variant="primary">Read More</Button>
          </Card.Body>
        </Card>
                    </Col>
    
                </Row>

                <Row className='py-4'>
                
                <Col md={6}>
                    <Card style={{ width: '23rem' }}>
          <Card.Img className='img-fluid' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/articles/11_370X370_crop_center.jpg?v=1535885739" />
          <Card.Body>
            
            <Card.Text>
                <b>TEMPORIBUS AUTEM QUIBUSDAM</b>
            <p className='py-2'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi...
            </p>
            </Card.Text>
            <Button variant="primary">Read More</Button>
          </Card.Body>
        </Card>
                    </Col>
    
                    <Col md={6}>
                    <Card style={{ width: '23rem' }}>
          <Card.Img className='img-fluid' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/articles/8_370X370_crop_center.jpg?v=1535885711" />
          <Card.Body>
            
            <Card.Text>
                <b>TEMPORIBUS AUTEM QUIBUSDAM</b>
            <p className='py-2'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi...
            </p>
            </Card.Text>
            <Button variant="primary">Read More</Button>
          </Card.Body>
        </Card>
                    </Col>
    
                </Row>
        </Col>
      </Row>
     
    </Container>
        
        
        </>
    )
}