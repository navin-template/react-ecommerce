import Carousel from 'react-bootstrap/Carousel';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import ProductList from './ProductList';


function MainSlider() {
  return (
    <>
    <div id='main'>
    <Carousel fade>
       <Carousel.Item>
      <Container className='my-4'>
      <Row className='py-4'>
        <Col  xs={12} md={6}><img src="https://ponno-demo.myshopify.com/cdn/shop/files/s1_1.png?v=1613171509" alt="" className='w-75 h-100'/></Col>
        <Col  xs={12} md={6} className='py-4'>
        <h6>Best Products</h6>
        <h1>Mount Carved 2200XD</h1>
        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature.</p>
        <Button variant="primary" className='my-4'>Shop Now</Button>{' '}
        </Col>
      </Row>
     
    </Container>
       
       
      </Carousel.Item>
      <Carousel.Item>
      <Container className='my-4'>
      <Row className='py-4'>
        <Col  xs={12} md={6}><img src="//ponno-demo.myshopify.com/cdn/shop/files/s1_2.png?v=1613171509" alt="" className='w-75 h-100'/></Col>
        <Col  xs={12} md={6} className='py-4'>
        <h6>Best Products</h6>
        <h1>Mount Carved 2200XD</h1>
        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature.</p>
        <Button variant="primary" className='my-4'>Shop Now</Button>{' '}
        </Col>
      </Row>
     
    </Container>
       
       
      </Carousel.Item>
    </Carousel>
    </div>
    <Container className='my-4'>
      <Row className='py-4'>
       
        <Col xs={12} md={8}>
        <div className="menu">
            <h6>Hot Deal</h6>
            <h2>Kemioo DX24 Headphone</h2>
            <p>Contrary to popular belief, Lorem Ipsum is not simply <br/> random text. It has roots in a piece of classical Latin  <br/> literature</p>
            </div>
        </Col>
        <Col  xs={12} md={4}>
          <Card className='product-box'>
            <div className='wishlist'>
            <i class="fa-regular fa-heart" style={{color: "rgb(32, 100, 217)"}}></i>
            </div>
            <di className="sidebar">
            <i class="fa-solid fa-sliders fa-rotate-90"></i>
            <i class="fa-regular fa-eye"></i>
            </di>
            <Card.Img className='hover-hide' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/7_f6cb367d-9d78-4395-a651-0aab92720a0c_270X270_crop_center.png?v=1536039112" />
            <Card.Img className='hover-show' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/4_6d126974-7768-49b0-bf41-d5ae8bc0ee44_270X270_crop_center.png?v=1536038081" />
            <Card.Body>
              <Card.Text className='text-center'>
              <p> bulk of the card's content.</p>
              <p> $ 1500.00</p>
              <p className='hover-hide'>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-regular fa-star" style={{color: "#0011ff"}}></i>
              </p>
              <p className='hover-show'><Button variant="light">ADD TO CART</Button></p>
              </Card.Text>
            
            </Card.Body>
          </Card>
     
        </Col>
      </Row>
      
    </Container>

    
    <Container className='my-4'>
    <div id='mainu'>
        <ProductList/>
    </div>
      
      
      <Container>
      

      <Row>
        <Col xs={12} md={6}>
            <div className='Contrary '>

            </div>
        </Col>
        <Col  xs={12} md={6}>
            <div className='popular'>
                
            </div>
        </Col>
      </Row>
    </Container>

      
      
    </Container>
    <Container className='my-4'>
    <Row>
        <Col xs={12} md={6} className=''>
          <h5>Laptops & Computers</h5>
            
        </Col>
        <Col  xs={12} md={6} className='texr-right'>
        <p > <samp style={{color:'#0438a1', marginRight:'10px', marginLeft:'15%',paddingLeft:'20%' }}> Featured</samp> New Product <samp style={{paddingLeft: '2%'}}> Best Deal</samp>
</p>
            
                
            
        </Col>
      </Row>
      <Row>
      <Col xs={12} md={4}>
        
      <Row className='bg-light'>
      <Col xs={12} md={4} className='p-0'>
        <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/products/3_27fb7bc8-f509-49b9-b571-965fcf57e01c_1024x1024.png?v=1536038080' alt='' w-50/>
      </Col>
        
        <Col className='py-5' xs={12} md={6}>
          <p>aliquam <br/> consequat mattis</p>
          <p>$ 150</p>
          <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
        
        </Col>
      </Row>
      </Col>
        <Col xs={12} md={4}>
            <Row className='bg-light mx-2'>
      <Col xs={12} md={5}className='py-5'>
        <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/products/4_88b44f2c-6aa4-4a4b-95d7-74479b1e4b19_grande.png?v=1536039097' alt='' />
      </Col>
        
        <Col className='py-5' xs={12} md={5}>
          <p>Product name  </p>
          <p>$ 30</p>
          <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
        
        </Col>
      </Row>
        </Col>
        <Col xs={12} md={4}>
           <Row className='bg-light'>
      <Col xs={12} md={6} className='p-0'>
        <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/products/5_1d8d4879-e55d-4288-b74f-a03e180fa358_grande.png?v=1536039104' alt='' w-50/>
      </Col>
        
        <Col className='py-5' xs={12} md={6}>
          <p>dummy Product <br/> Title </p>
          <p>$ 120</p>
          <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
        
        </Col>
      </Row>
        </Col>
      </Row>
     
    </Container>
    <Container className='my-4'>
      <Row>
      <Col xs={12} md={4}>
        
      <Row className='bg-light'>
      <Col xs={12} md={6} className='p-0'>
        <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/products/8_2ebd101f-16aa-432e-a25f-945cbc09230e_grande.png?v=1536038214' alt='' w-50/>
      </Col>
        
        <Col className='py-5' xs={12} md={6}>
          <p>cras neque metus</p>
          <p>$ 30</p>
          <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
        
        </Col>
      </Row>
      </Col>
        <Col xs={12} md={4}>
        <Row className='bg-light  mx-2'>
      <Col xs={12} md={6} className='p-0'>
        <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/products/3_27fb7bc8-f509-49b9-b571-965fcf57e01c_1024x1024.png?v=1536038080' alt='' w-50/>
      </Col>
        
        <Col className='py-5' xs={12} md={6}>
          <p>aliquam <br/> consequat mattis</p>
          <p>$ 150</p>
          <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
        
        </Col>
      </Row>
        </Col>
        <Col xs={12} md={4}>
           <Row className='bg-light'>
      <Col xs={12} md={6}className='p-0'>
        <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/products/1_grande.png?v=1536038068' alt='' w-50/>
      </Col>
        
        <Col className='py-5' xs={12} md={6}>
          <p>Accusantium <br/> doloremque </p>
          <p>$ 120</p>
          <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
        
        </Col>
      </Row>
        </Col>
      </Row>
     
    </Container>
    <Container className='my-4'>
      <Row>
      <Col xs={12} md={4}>
        
      <Row className='bg-light'>
      <Col xs={12} md={6} className='p-0'>
        <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/products/14_c72d180e-460a-4275-9373-0b1fee5c9e03_grande.png?v=1536038535' alt='' w-50/>
      </Col>
        
        <Col className='py-5' xs={12} md={6}>
          <p>Donec ac tempus <br/> nrb</p>
          <p>$ 30</p>
          <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
        
        </Col>
      </Row>
      </Col>
        <Col xs={12} md={4}>
            <Row className='bg-light mx-2'>
      <Col xs={12} md={5} className='p-0 '>
        <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/products/5_9e76c003-e4c3-4e50-9049-2e0dc41268de_grande.png?v=1536038093' alt='' w-50/>
      </Col>
        
        <Col className='py-5' xs={12} md={5} >
          <p>Carry Bag of <br/> Leather </p>
          <p>$ 80</p>
          <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
        
        </Col>
      </Row>
        </Col>
        <Col xs={12} md={4} >
           <Row className='bg-light'>
      <Col xs={12} md={6}  className='p-0'>
        <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/products/2_d0cf719e-7944-4fe5-acfd-c59257777bf4_grande.png?v=1536038811' alt='' w-50/>
      </Col>
        
        <Col className='py-5' xs={12} md={6} >
          <p>Dummy Product <br/> Name </p>
          <p>$ 56.20</p>
          <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
        
        </Col>
      </Row>
        </Col>
      </Row>
     
    </Container>


    <Container fluid="md" className='my-4'>
      <Row>
        <Col xs={12}>
        <img src='https://ponno-demo.myshopify.com/cdn/shop/files/b4.jpg?v=1613171901' alt=''/>
        </Col>
      </Row>
    </Container>


   
    <Container className='my-4'>
    <Row>
      <Col xs={12} md={4}  className=''>
      
        
      <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/files/b5.jpg?v=1613171902' alt=''style={{height:'420px'}}/>
     
      </Col>
        <Col xs={12} md={4} >
        <Row >
      <Col >
        <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/files/b6.jpg?v=1613171902' alt='' />
      </Col>
        </Row>
        </Col>
        <Col xs={12} md={4} >
        <h5 className='py-4'>On Sale</h5>
        <Row className='bg-light  '>
        
      <Col xs={12} md={6}  className='p-0'>
        <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/products/3_27fb7bc8-f509-49b9-b571-965fcf57e01c_1024x1024.png?v=1536038080' alt='' w-50/>
      </Col>
        
        <Col className='py-5' xs={12} md={6} >
          <p>aliquam <br/> consequat mattis</p>
          <p>$ 150</p>
          <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
        
        </Col>
      </Row>
      <Row className='bg-light my-2'>
      <Col xs={12} md={6}  >
        <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/products/4_88b44f2c-6aa4-4a4b-95d7-74479b1e4b19_grande.png?v=1536039097' alt='' />
      </Col>
        
        <Col className='' xs={12} md={6} >
          <p>Product name  </p>
          <p>$ 30</p>
          <p><i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star" style={{color: '#1b59c5'}}></i> <i class="fa-solid fa-star-half-stroke" style={{color: '#185bcd'}}></i></p>
        
        </Col>
      </Row>
        </Col>
      </Row>
     
    </Container>

 
  </>
  );
}

export default MainSlider;