//import logo from './logo.svg';

import './App.css';
import { BrowserRouter as Router, Routes, Route,  Outlet,  Link } from 'react-router-dom'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Home from './Home';
import MainMenu from './MainMenu';
import MainSlider from './MainSlider';
import Shop from './Shop';
import Product from './Product';
import Blog from './Blog';
import Login from './Login';
import SignUp from './Signup';

function App() {
  return (
  <>
   <Home />
  <MainMenu/>
  
  
  <Routes>

  
          <Route path="/" element={<MainSlider />} />
          
          <Route path="/shop" element={<Shop />} />
          <Route path="/product" element={<Product />} />
          <Route path='/blog' element={<Blog/>} />
        <Route path="/login" element={<Login/>} />
        <Route path="/signup" element={<SignUp/>} />
        
       
        
    </Routes>
  
    <Outlet />
    
    <div className=' literature'>
<p><img src='https://ponno-demo.myshopify.com/cdn/shop/files/logo2_300x300.png?v=1613171497' alt=''/></p>
<p className='py-4'>Contrary to popular belief, Lorem Ipsum is not simply random <br/> text. It has roots in a piece of classical Latin literature.</p>
<Container className='py-4'>
          <Row>
        <Col>
        <Row>
          <Col xs={6} md={2} >
            <img src='https://ponno-demo.myshopify.com/cdn/shop/files/s1_compact.png?v=1613171497'/>
          </Col>
          <Col xs={6} md={10} >
            <h6>Free Shipping</h6>
            <p>Most product are free<br/>
shipping.</p>
          </Col>
        </Row>
        
        </Col>
        <Col>
        <Row>
          <Col xs={6} md={2}>
            <img src='https://ponno-demo.myshopify.com/cdn/shop/files/s2_compact.png?v=1613171497'/>
          </Col>
          <Col xs={6} md={10}>
            <h6>Customer Support</h6>
            <p>24x7 Customer Support</p>
          </Col>
        </Row>
        </Col>
        <Col>
        <Row>
          <Col xs={6} md={2}>
            <img src='https://ponno-demo.myshopify.com/cdn/shop/files/s3_compact.png?v=1613171497'/>
          </Col>
          <Col xs={6} md={10}>
            <h6>Secure Payment</h6>
            <p>Most Secure Payment <br/>for customer.</p>
          
          </Col>
        </Row>
        </Col>
      </Row>
      <hr className='py-4'/>
    </Container>
    <Container className='py-4'>
     
      <Row>
        <Col xs={12} md={3}>
          <h6 className='py-4'>Recent Post</h6>
          <Row>
            <Col xs={12} md={4}>
              <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/articles/12_120X120_crop_center.jpg?v=1535885748' alt=''/>
            </Col>
            <Col xs={12} md={8}>
              <p className='text-left'>Temporibus autem</p>
              <p className='text-left'>quibusdam</p>
              <p className='text-left'>Sep 02 , 2018</p>
            </Col>
          </Row>
          <Row className='py-4'>
            <Col xs={12} md={4}>
              <img className='img-fluid' src='https://ponno-demo.myshopify.com/cdn/shop/articles/9_120X120_crop_center.jpg?v=1535885720' alt=''/>
            </Col>
            <Col xs={12} md={8}>
              <p className='text-left'>Temporibus autem</p>
              <p className='text-left'>quibusdam</p>
              <p className='text-left'>Sep 02 , 2018</p>
            </Col>
          </Row>
        </Col>
        <Col xs={12} md={3} className='text-left'>
          <h6>Quick Link</h6>
          <p className='py-2'>Cart</p>
          <p className='py-2'>Checkout</p>
          <p className='py-2'>Tracking</p>
          <p className='py-2'>Product</p>
          <p className='py-2'>Blog</p>
        </Col>
        <Col xs={12} md={3} className='text-left'>
          <h6>Contact Info</h6>
          <p className='py-2'><i class="fa-solid fa-location-dot" style={{color: '#f4f5f6'}}></i> lorem address south road <br/>
           77 north, USA -9991</p>
          <p className='py-2'><i class="fa-solid fa-envelope" style={{color: '#f4f5f6'}}></i> info@mail.com
           <br/>info@mail.com</p>
          <p className='py-2' > <i class="fa-solid fa-phone" style={{color: '#f4f5f6'}}></i> 123 456 789<br/>
           123 456 789</p>
         
        </Col>
        <Col xs={3} className='text-left'>
        <h6>Newsletter</h6>
        <p className='py-2'>Sed ut perspiciatis unde omnis iste</p>
        <p className='py-2'>natus error sit voluptatem accusantium</p>
        <p className='py-2'>doloremque laudantium.</p>
        <Form>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" />
        <Form.Text className="text-muted">
         
        </Form.Text>
      </Form.Group>

      
     
    </Form>
       <h6>Social Icon</h6>

        </Col>
       
      </Row>
    </Container> 
    
    
    
    
    </div>
  </>
  );
}

export default App;
