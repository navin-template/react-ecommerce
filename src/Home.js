import { render } from "@testing-library/react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';


export default function Home(){
    return(
        <>
        <div id="hero">
        <Container className="my-4">
      <Row className="py-4">
        <Col  xs={12} md={4}><img src="https://ponno-demo.myshopify.com/cdn/shop/files/logo_300x300.png?v=1613171496" alt=""/></Col>
        <Col xs={12} md={6}> <InputGroup className="mb-3">
        <Form.Control
          placeholder="Recipient's username"
          aria-label="Recipient's username"
          aria-describedby="basic-addon2"
        />
        <InputGroup.Text id="basic-addon2"><i className="fa-solid fa-magnifying-glass px-4"></i></InputGroup.Text>
      </InputGroup></Col>
        <Col  xs={12} md={2}>
          
          <Row className="py-1" style={{backgroundColor:'#0438a1'}}>
            <Col md={4}> <i class="fa-brands fa-shopify" style={{color: '#fff', fontSize:'50px'}}></i></Col>
            <Col md={8} style={{color: '#fff'}}> <p>0 item <br/>$0.00</p></Col>
          </Row>
          </Col>
      </Row>
    </Container>
    </div>
        </>

    )
}