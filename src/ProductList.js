import Carousel from 'react-multi-carousel';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import 'react-multi-carousel/lib/styles.css';
const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 4
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 4
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };
function ProductList(){
    return(
        <>
            <Carousel responsive={responsive} itemClass="px-1">
                <div>
                <Card className='product-box'>
                    <div className='wishlist'>
                    <i class="fa-regular fa-heart" style={{color: "rgb(32, 100, 217)"}}></i>
                    </div>
                    <di className="sidebar">
                    <i class="fa-solid fa-sliders fa-rotate-90"></i>
                    <i class="fa-regular fa-eye"></i>
                    </di>
                    <Card.Img className='hover-hide' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/10_fd74d87b-9fb0-44df-b291-d5bef99d99f0_270X270_crop_center.png?v=1536039131" />
                    <Card.Img className='hover-show' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/7_f6cb367d-9d78-4395-a651-0aab92720a0c_270X270_crop_center.png?v=1536039112" />
                    <Card.Body>
                    <Card.Text className='text-center'>
                    <p> bulk of the card's content.</p>
                    <p> $ 120.00</p>
                    <p className='hover-hide'>
                    <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
                    <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
                    <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
                    <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
                    <i class="fa-regular fa-star" style={{color: "#0011ff"}}></i>
                    </p>
                    <p className='hover-show'><Button variant="light">ADD TO CART</Button></p>
                    </Card.Text>
                    
                    </Card.Body>
                </Card>
                </div>
                <div>
                <Card className='product-box'>
            <div className='wishlist'>
            <i class="fa-regular fa-heart" style={{color: "rgb(32, 100, 217)"}}></i>
            </div>
            <di className="sidebar">
            <i class="fa-solid fa-sliders fa-rotate-90"></i>
            <i class="fa-regular fa-eye"></i>
            </di>
            <Card.Img className='hover-hide' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/7_f6cb367d-9d78-4395-a651-0aab92720a0c_270X270_crop_center.png?v=1536039112" />
            <Card.Img className='hover-show' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/4_6d126974-7768-49b0-bf41-d5ae8bc0ee44_270X270_crop_center.png?v=1536038081" />
            <Card.Body>
              <Card.Text className='text-center'>
              <p> bulk of the card's content.</p>
              <p> $ 1500.00</p>
              <p className='hover-hide'>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-regular fa-star" style={{color: "#0011ff"}}></i>
              </p>
              <p className='hover-show'><Button variant="light">ADD TO CART</Button></p>
              </Card.Text>
            
            </Card.Body>
          </Card>
                </div>
                <div>
                <Card className='product-box'>
            <div className='wishlist'>
            <i class="fa-regular fa-heart" style={{color: "rgb(32, 100, 217)"}}></i>
            </div>
            <di className="sidebar">
            <i class="fa-solid fa-sliders fa-rotate-90"></i>
            <i class="fa-regular fa-eye"></i>
            </di>
            <Card.Img className='hover-hide' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/6_68b5568a-7172-4de3-a436-dd199022098b_270X270_crop_center.png?v=1536039105" />
            <Card.Img className='hover-show' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/10_fd74d87b-9fb0-44df-b291-d5bef99d99f0_270X270_crop_center.png?v=1536039131" />
            <Card.Body>
              <Card.Text className='text-center'>
              <p> bulk of the card's content.</p>
              <p> $ 90.00</p>
              <p className='hover-hide'>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-regular fa-star" style={{color: "#0011ff"}}></i>
              </p>
              <p className='hover-show'><Button variant="light">ADD TO CART</Button></p>
              </Card.Text>
            
            </Card.Body>
          </Card>
                </div>
                <div>
                <Card className='product-box'>
            <div className='wishlist'>
            <i class="fa-regular fa-heart" style={{color: "rgb(32, 100, 217)"}}></i>
            </div>
            <di className="sidebar">
            <i class="fa-solid fa-sliders fa-rotate-90"></i>
            <i class="fa-regular fa-eye"></i>
            </di>
            <Card.Img className='hover-hide' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/10_fd74d87b-9fb0-44df-b291-d5bef99d99f0_270X270_crop_center.png?v=1536039131" />
            <Card.Img className='hover-show' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/7_f6cb367d-9d78-4395-a651-0aab92720a0c_270X270_crop_center.png?v=1536039112" />
            <Card.Body>
              <Card.Text className='text-center'>
              <p> bulk of the card's content.</p>
              <p> $ 120.00</p>
              <p className='hover-hide'>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-regular fa-star" style={{color: "#0011ff"}}></i>
              </p>
              <p className='hover-show'><Button variant="light">ADD TO CART</Button></p>
              </Card.Text>
            
            </Card.Body>
          </Card>
                </div>
                <div>
                <Card className='product-box'>
            <div className='wishlist'>
            <i class="fa-regular fa-heart" style={{color: "rgb(32, 100, 217)"}}></i>
            </div>
            <di className="sidebar">
            <i class="fa-solid fa-sliders fa-rotate-90"></i>
            <i class="fa-regular fa-eye"></i>
            </di>
            <Card.Img className='hover-hide' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/5_1d8d4879-e55d-4288-b74f-a03e180fa358_270X270_crop_center.png?v=1536039104" />
            <Card.Img className='hover-show' variant="top" src="https://ponno-demo.myshopify.com/cdn/shop/products/14_e94de152-b927-4a9d-9113-54ec69f78c88_270X270_crop_center.png?v=1536039349" />
            <Card.Body>
              <Card.Text className='text-center'>
              <p> bulk of the card's content.</p>
              <p> $ 90.00</p>
              <p className='hover-hide'>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-solid fa-star" style={{color: "#0011ff"}}></i>
              <i class="fa-regular fa-star" style={{color: "#0011ff"}}></i>
              </p>
              <p className='hover-show'><Button variant="light">ADD TO CART</Button></p>
              </Card.Text>
            
            </Card.Body>
          </Card>
                </div>
            </Carousel>
        </>
    );
}

export default ProductList;
